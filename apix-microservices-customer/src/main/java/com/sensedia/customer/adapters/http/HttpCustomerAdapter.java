package com.sensedia.customer.adapters.http;

import com.sensedia.customer.adapters.dtos.CustomerCreationDto;
import com.sensedia.customer.adapters.dtos.CustomerDto;
import com.sensedia.customer.adapters.mappers.CustomerMapper;
import com.sensedia.customer.domains.Customer;
import com.sensedia.customer.ports.ApplicationPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("${base-path}/customers")
public class HttpCustomerAdapter {

    private final ApplicationPort customerApplication;
    private final CustomerMapper customerMapper;

    @Value("${base-path}")
    private String basePath;

    @Autowired
    public HttpCustomerAdapter(
            ApplicationPort customerApplication, CustomerMapper customerMapper) {
        this.customerApplication = customerApplication;
        this.customerMapper = customerMapper;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody CustomerCreationDto customerCreationDto) {
        // Transferindo para DTO (Data Transfer Object)
        // Para separarmos os objetos de domínio das camadas
        Customer customerMapper = this.customerMapper.toCustomer(customerCreationDto);

        // Agora com os valores do objeto de DTO dentro do nosso domínio,
        // passamos os valores para o nosso core.
        Customer customer = customerApplication.create(customerMapper);

        // Respondendo a requisição com o status 201 e no header response o
        // o location da nossa entidade, método GET e url ${nosso_path}/customer/12345
        return ResponseEntity.created(buildLocation(customer.getId())).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> get(@PathVariable String id) {
        Customer customer = customerApplication.findById(id);

        CustomerDto customerDto = customerMapper.toCustomerDto(customer);

        return ResponseEntity.ok(customerDto);
    }
//gabrielandrade-customers/1.0/customers
//gabrielandrade-notifications/1.0/notifications
    private URI buildLocation(String id) {
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path(basePath + "/customers/{id}")
                .buildAndExpand(id)
                .toUri();
    }
}
