package com.sensedia.notification.adapters.amqp.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface BrokerOutput {

  @Output(BindConfig.PUBLISH_NOTIFICATION_OPERATION_ERROR)
  MessageChannel publishNotificationOperationError();
}
