package ${package}.commons.errors.resolvers;

import ${package}.commons.errors.domains.DefaultErrorResponse;
import ${package}.commons.errors.exceptions.ApplicationException;
import org.springframework.stereotype.Service;

@Service
public class ApplicationExceptionResolver implements Resolver<ApplicationException> {

  @Override
  public DefaultErrorResponse getErrorResponse(ApplicationException e) {
    return e.getDefaultErrorResponse();
  }
}
